/*
 * Copyright 2012 Vincent Lhote
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;

import net.redlightning.pcgen.R;

/**
 * A dialog with a Ok, a cancel and eventually an apply button.
 */
public abstract class AbstractDialog extends AlertDialog
{
	// TODO provide a UIManager or L&F derivated value
	protected static final int GAP = 12;

	public AbstractDialog(View f, String title, boolean modal)
	{
	    super(f.getContext());
	    setTitle(title);
	    setCancelable(modal);
		initialize(f.getContext());
	}

	private void initialize(Context context)
	{
        OnClickListener okListener = (dialogInterface, viewID) ->  okButtonActionPerformed();
		this.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.in_ok), okListener);

        OnClickListener cancelListener = (dialogInterface, viewID) ->  cancelButtonActionPerformed();
        this.setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.in_cancel), cancelListener);

        OnClickListener applyListener = (dialogInterface, viewID) ->  applyButtonActionPerformed();
        this.setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.in_cancel), applyListener);
	}

	/**
	 * Set the ok default button as default. Use for dialog that need some field to be entered before allowing ok to be
	 * the default, like a username/password dialog.
	 */
	protected void setOkAsDefault()
	{
		//Ignored in Android version
	}

	/**
	 * {@code true} if the ok button should be set as default during init
	 * @return {@code true} by default
	 */
	protected boolean shouldSetOkAsDefault()
	{
		return true;
	}

	/**
	 * Indicate if Esc should be installed as close window (not cancel) during init
	 * @return {@code true} by default
	 */
	protected boolean shouldInstallEsc()
	{
		return true;
	}

	protected String getCancelMnKey()
	{
		return "in_mn_cancel"; //$NON-NLS-1$
	}

	protected String getCancelKey()
	{
		return "in_cancel"; //$NON-NLS-1$
	}

	protected String getOkMnKey()
	{
		return "in_mn_ok"; //$NON-NLS-1$
	}

	protected String getOkKey()
	{
		return "in_ok"; //$NON-NLS-1$
	}

	protected abstract View getCenter();

	protected boolean includeApplyButton()
	{
		return false;
	}

	/**
	 * Defaults to calling apply and closing.
	 */
	public void okButtonActionPerformed()
	{
		applyButtonActionPerformed();
		close();
	}

	/**
	 * Defaults to closing the window
	 */
	public void cancelButtonActionPerformed()
	{
		close();
	}

	/**
	 * Defaults to hide and dispose.
	 */
	protected void close()
	{
	    cancel();
	}

	/**
	 * what to do if the ok button is pressed (beside closing the dialog)
	 */
	protected abstract void applyButtonActionPerformed();

}
