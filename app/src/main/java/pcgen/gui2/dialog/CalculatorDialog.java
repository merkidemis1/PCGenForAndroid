/*
 * Copyright 2011 Stefan Radermacher <zaister@users.sourceforge.net>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import net.redlightning.pcgen.R;

import pcgen.core.VariableProcessor;
import pcgen.facade.core.CharacterFacade;
import pcgen.gui2.PCGenFrame;
import pcgen.system.LanguageBundle;

/**
 * A dialog to allow character variables and expressions to be evaluated 
 * interactively by the user.
 * 
 */
public class CalculatorDialog extends AlertDialog
{
	private final PCGenFrame pcgenFrame;
	private final FormulaPanel formulaPanel;
	private final EditText outputText;

	public CalculatorDialog(PCGenFrame parent)
	{
		super(parent.getContext());
		this.pcgenFrame = parent;
		setTitle(LanguageBundle.getString("in_mnuToolsCalculator"));
		outputText = new EditText(parent.getContext());
		formulaPanel = new FormulaPanel(this.getContext(), outputText);
		initComponents(parent.getContext());
	}

	private void initComponents(Context context)
	{
        final LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

		outputText.setEnabled(false);
		layout.addView(formulaPanel);
	 
	    final ScrollView js = new ScrollView(context);
	    js.addView(outputText);
	    layout.addView(js);
	}

	private class ButtonPanel extends View
	{
		private final Button calcButton;
		private final Button clearButton;
		private final TextView formulaText;
		private final EditText outputText;
		
		public ButtonPanel(TextView formulaText, EditText outputText)
		{
		    super(pcgenFrame.getContext());
            this.formulaText = formulaText;
            this.outputText = outputText;

            calcButton = new Button(pcgenFrame.getContext());
			calcButton.setText(R.string.in_calculate);

			clearButton = new Button(pcgenFrame.getContext());
			clearButton.setText(R.string.in_clear);

			initComponents();
		}
		
		private void initComponents()
		{
            clearButton.setOnClickListener((View v) -> outputText.setText(""));

            calcButton.setOnClickListener((View v) -> {
                CharSequence formula = formulaText.getText();
                CharacterFacade currentPC = pcgenFrame.getSelectedCharacterRef().get();

                if (currentPC != null)
                {
                    VariableProcessor vp = currentPC.getVariableProcessor();
                    vp.pauseCache();
                    outputText.append(currentPC.getNameRef() + ": " + formula + " = "
                            + currentPC.getVariable(formula.toString(), true) + "\n");
                    vp.restartCache();
                }
                else
                {
                    outputText.append("No character currently selected.\n");
                }
                formulaText.requestFocus();
            });
		}
	}
	
	private class FormulaPanel extends View
	{
	    private final RelativeLayout layout;
		private final TextView formulaText;
		private final ButtonPanel buttonPanel;
		
		public FormulaPanel(Context context, EditText outputText)
		{
		    super(context);
		    layout = new RelativeLayout(context);
			formulaText = new TextView(context);
			buttonPanel = new ButtonPanel(formulaText, outputText);
			layout.addView(formulaText);
			layout.addView(buttonPanel);
		}
	}
}
