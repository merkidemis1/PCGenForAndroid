/*
 * Copyright James Dempsey, 2013
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.view.View;

import net.redlightning.pcgen.R;

import pcgen.facade.core.SpellBuilderFacade;
import pcgen.gui2.equip.SpellChoicePanel;


/**
 * The Class {@code SpellChoiceDialog} provides a pop-up dialog that allows
 * the user to select a spell for inclusion in things like custom equipment 
 * items.  
 *
 * 
 */
public class SpellChoiceDialog extends AlertDialog {
	/**
	 * Create a new instance of SpellChoiceDialog
	 * @param frame The parent frame we are displaying over.
	 */
	public SpellChoiceDialog(View frame, SpellBuilderFacade builder)
	{
		super(frame.getContext());
		setTitle(R.string.in_csdChooseSpell); //$NON-NLS-1$
        SpellChoicePanel spellChoicePanel = new SpellChoicePanel(frame.getContext(), builder);
		this.setContentView(spellChoicePanel);
		setCancelable(true);
	}
}
