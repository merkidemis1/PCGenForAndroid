/*
 * Copyright James Dempsey, 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.dialog;

import android.app.AlertDialog;
import android.view.View;

import net.redlightning.pcgen.R;

import java.io.File;

import pcgen.core.SettingsHandler;
import pcgen.gui2.doomsdaybook.NameGenPanel;

/**
 * The Class {@code RandomNameDialog} is a dialog in which the user can
 * generate a random name for their character.
 *
 * 
 */
@SuppressWarnings("serial")
public class RandomNameDialog extends AlertDialog
{
	private final NameGenPanel nameGenPanel;

	/**
	 * Create a new Random Name Dialog
	 * @param frame The parent frame. The dialog will be centred on this frame
	 * @param gender The current gender of the character.
	 */
	public RandomNameDialog(View frame, String gender)
	{
	    super(frame.getContext());
	    this.setTitle(R.string.in_rndNameTitle);
		nameGenPanel = new NameGenPanel(frame.getContext(), new File(getDataDir()));
		nameGenPanel.setGender(gender);
		this.setContentView(nameGenPanel);
	}

	/**
	 * @return The directory where the random name data is held
	 */
	private String getDataDir()
	{
		String pluginDirectory = SettingsHandler.getGmgenPluginDir().toString();

		return pluginDirectory + File.separator + "Random Names";
	}
	
	/**
	 * @return The name the user generated.
	 */
	public String getChosenName()
	{
		return nameGenPanel.getChosenName();
	}

	/**
	 * @return the gender
	 */
	public String getGender()
	{
		return nameGenPanel.getGender();
	}
}
