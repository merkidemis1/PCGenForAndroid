package pcgen.gui2.dialog.about

import android.app.AlertDialog
import android.content.Context
import android.support.v4.view.ViewPager
import net.redlightning.pcgen.R

//extends AlertDialog
class AboutDialog(context: Context) : AlertDialog(context) {

    init {
        this.setTitle(R.string.in_abt_title)
//        getContentPane().setLayout(BorderLayout())
//        getContentPane().add(MainAbout(), BorderLayout.CENTER)
//        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
//        Utility.setComponentRelativeLocation(frame, this)
//        Utility.installEscapeCloseOperation(this)

        setContentView(R.layout.dialog_about);

        // Instantiate a ViewPager and a PagerAdapter.
        val mPager = this.findViewById<ViewPager>(R.id.about_pager)
        val mPagerAdapter =  ScreenSlidePagerAdapter(context.getSupportFragmentManager())
        mPager.setAdapter(mPagerAdapter);
    }

}