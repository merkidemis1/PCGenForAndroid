package pcgen.gui2.dialog.about

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class AboutPagerAdapter(fragmentManager: FragmentManager, private val tabs: ArrayList<Fragment>) :
            FragmentStatePagerAdapter(fragmentManager) {
    
        override fun getItem(position: Int): Fragment {
            return CreditsFragment.newInstance(tabs[position])
        }

        override fun getCount(): Int {
            return tabs.size
        }
    }
}