/*
 * Copyright 2003 (C) Devon Jones
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package pcgen.gui2.doomsdaybook;

import org.jdom2.DataConversionException;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.prefs.Preferences;

import gmgen.util.LogUtilities;
import pcgen.core.doomsdaybook.CRRule;
import pcgen.core.doomsdaybook.DataElement;
import pcgen.core.doomsdaybook.DataElementComperator;
import pcgen.core.doomsdaybook.DataValue;
import pcgen.core.doomsdaybook.HyphenRule;
import pcgen.core.doomsdaybook.Rule;
import pcgen.core.doomsdaybook.RuleSet;
import pcgen.core.doomsdaybook.SpaceRule;
import pcgen.core.doomsdaybook.VariableHashMap;
import pcgen.core.doomsdaybook.WeightedDataValue;
import pcgen.system.LanguageBundle;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.redlightning.pcgen.R;

import plugin.doomsdaybook.RandomNamePlugin;

/**
 * Main panel of the random name generator.
 */
public class NameGenPanel extends View {
    private static final String TAG = NameGenPanel.class.getCanonicalName();
    private final Preferences namePrefs = Preferences.userNodeForPackage(NameGenPanel.class);
    private final Map<String, List<RuleSet>> categories = new HashMap<>();

    private ArrayAdapter<CharSequence> genderAdapter;
    private Button generateButton;
    private Button copyButton;
    private CheckBox chkStructure;
    private Spinner cbCatalog;
    private Spinner cbCategory;
    private Spinner cbSex;
    private Spinner cbStructure;
    private TextView meaning;
    private EditText name;
    private final VariableHashMap allVars = new VariableHashMap();
    private Rule lastRule = null;

    /**
     * Creates new form NameGenPanel
     */
    public NameGenPanel(Context context) {
        super(context);
        initComponents(context);
        initPrefs();
        loadData(context, new File("."));
    }

    /**
     * Constructs a NameGenPanel given a dataPath
     *
     * @param dataPath The path to the random name data files.
     */
    public NameGenPanel(Context context, File dataPath) {
        super(context);
        initComponents(context);
        initPrefs();
        loadData(context, dataPath);
    }

    /**
     * Generate a Rule
     *
     * @return new Rule
     */
    public Rule generate() {
        try {
            Rule rule;

            if (chkStructure.isSelected()) {
                RuleSet rs = (RuleSet) cbCatalog.getSelectedItem();
                rule = rs.getRule();
            } else {
                rule = (Rule) cbStructure.getSelectedItem();
            }

            ArrayList<DataValue> aName = rule.getData();
            setNameText(aName);
            setMeaningText(aName);

            return rule;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
    }

    /**
     * Initialization of the bulk of preferences.  sets the defaults
     * if this is the first time you have used this version
     */
    private void initPrefs() {
        boolean prefsSet = namePrefs.getBoolean("arePrefsSet", false);

        if (!prefsSet) {
            namePrefs.putBoolean("arePrefsSet", true);
        }

        double version = namePrefs.getDouble("Version", 0);

        if ((version < 0.5) || !prefsSet) {
            namePrefs.putDouble("Version", 0.5);
        }

        namePrefs.putDouble("SubVersion", 0);
    }

    private void setMeaningText(String meaning) {
        this.meaning.setText(meaning);
    }

    private void setMeaningText(Iterable<DataValue> data) {
        StringBuilder meaningBuffer = new StringBuilder();

        for (DataValue val : data) {
            String aMeaning = val.getSubValue("meaning"); //$NON-NLS-1$ // XML attribute no translation

            if (aMeaning == null) {
                aMeaning = val.getValue();
            }

            meaningBuffer.append(aMeaning);
        }

        setMeaningText(meaningBuffer.toString());
    }

    private void setNameText(String name) {
        this.name.setText(name);
        LogUtilities.inst().logMessage(RandomNamePlugin.LOG_NAME, name);
    }

    private void setNameText(Iterable<DataValue> data) {
        StringBuilder nameBuffer = new StringBuilder();

        for (DataValue val : data) {
            nameBuffer.append(val.getValue());
        }

        setNameText(nameBuffer.toString());
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.dialog_random_name, null);
        genderAdapter = ArrayAdapter.createFromResource(context, R.array.genders, android.R.layout.simple_spinner_item);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbSex = layout.findViewById(R.id.name_gen_sex);
        cbSex.setAdapter(genderAdapter);

        cbCategory = layout.findViewById(R.id.name_gen_category);
        generateButton = layout.findViewById(R.id.name_gen_generate);
        cbStructure = layout.findViewById(R.id.name_gen_structure);
        chkStructure = layout.findViewById(R.id.name_gen_random_struct);
        meaning = layout.findViewById(R.id.name_gen_meaning);
        name = layout.findViewById(R.id.name_gen_name);
        copyButton = layout.findViewById(R.id.name_gen_copy_button);
        cbCatalog = layout.findViewById(R.id.name_gen_catalog);


        cbCategory.setOnItemClickListener((adapterView, view1, position, id) -> {
            loadGenderDD();
            loadCatalogDD(context);
            loadStructureDD(context);
        });

        cbCatalog.setOnItemClickListener((adapterView, view1, position, id) -> loadStructureDD(context));


        cbCategory.setOnItemClickListener((adapterView, view1, position, id) -> {
            this.loadGenderDD();
            loadCatalogDD(context);
            loadStructureDD(context);
        });


        generateButton.setOnClickListener((view) -> {
            try {
                Rule rule = this.lastRule;

                if (rule == null) {
                    if (chkStructure.isSelected()) {
                        RuleSet rs = (RuleSet) cbCatalog.getSelectedItem();
                        rule = rs.getLastRule();
                    } else {
                        rule = (Rule) cbStructure.getSelectedItem();
                    }

                    this.lastRule = rule;
                }

                ArrayList<DataValue> aName = rule.getLastData();

                setNameText(aName);
                setMeaningText(aName);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        });

        cbSex.setOnItemClickListener((adapterView, view1, position, id) -> {
            loadCatalogDD(context);
            loadStructureDD(context);
        });

        cbStructure.setEnabled(false);

        chkStructure.setSelected(true);
        chkStructure.setText(LanguageBundle.getString("in_randomButton")); //$NON-NLS-1$
        chkStructure.setOnCheckedChangeListener((view, isChecked) -> loadStructureDD(context));

        // Name display

        meaning.setText(LanguageBundle.getString("in_rndNmDefault")); //$NON-NLS-1$

        //Copy button
        copyButton.setOnClickListener((view) -> {
            ClipData clip = ClipData.newPlainText("name", name.getText());
            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setPrimaryClip(clip);
        });

    }

    private void loadCatalogDD(Context context) {
        try {
            String catKey = (String) cbCategory.getSelectedItem();
            String sexKey = (String) cbSex.getSelectedItem();
            RuleSet oldRS = (RuleSet) cbCatalog.getSelectedItem();
            String catalogKey = "";

            if (oldRS != null) {
                catalogKey = oldRS.getTitle();
            }

            List<RuleSet> cats = categories.get(catKey);
            List<RuleSet> sexes = categories.get("Sex: " + sexKey);
            List<RuleSet> join = new ArrayList<>(cats);
            join.retainAll(sexes);
            join.sort(new DataElementComperator());

            Vector<RuleSet> catalogs = new Vector<>();
            int oldSelected = -1;
            int n = 0;

            for (final RuleSet rs : join) {
                if (rs.getUsage().equals("final")) {
                    catalogs.add(rs);

                    if (rs.getTitle().equals(catalogKey)) {
                        oldSelected = n;
                    }

                    n++;
                }
            }

            ArrayAdapter<RuleSet> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
            adapter.addAll(catalogs);
            cbCatalog.setAdapter(adapter);
            if (oldSelected >= 0) {
                cbCatalog.setSelection(oldSelected);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    //	Get a list of all the gender categories in the category map
    private Vector<String> getGenderCategoryNames() {
        Vector<String> genders = new Vector<>();
        Set<String> keySet = categories.keySet();

        //	Loop through the keys in the categories
        for (final String key : keySet) {
            //	if the key starts with "Sex" then save it
            if (key.startsWith("Sex:")) {
                genders.add(key.substring(5));
            }
        }

        //	Return all the found gender types
        return genders;
    }

    //	Load the gender drop dowd
    private void loadGenderDD() {
        List<String> genders = getGenderCategoryNames();
        Vector<String> selectable = new Vector<>();
        String gender = (String) cbSex.getSelectedItem();

        //	Get the selected category name
        String category = (String) cbCategory.getSelectedItem();

        //	Get the set of rules for selected category
        List<RuleSet> categoryRules = categories.get(category);

        //	we need to determine if the selected category is supported by the
        //	available genders
        //	loop through the available genders
        for (String genderString : genders) {
            //	Get the list of rules for the current gender
            List<RuleSet> genderRules = categories.get("Sex: " + genderString);

            //	now loop through all the rules from the selected category
            for (RuleSet categoryRule : categoryRules) {
                //	if the category rule is in the list of gender rules
                //	add the current gender to the selectable gender list
                //	we can stop processing the list once we find a match
                if (genderRules.contains(categoryRule)) {
                    selectable.add(genderString);
                    break;
                }
            }
        }

        //	Sort the genders
        Collections.sort(selectable);

        genderAdapter.clear();
        genderAdapter.addAll(selectable);
        genderAdapter.notifyDataSetChanged();
    }

    private void loadCategory(Element category, RuleSet rs) {
        List<RuleSet> cat = categories.get(category.getAttributeValue("title"));
        List<RuleSet> thiscat;

        if (cat == null) {
            thiscat = new ArrayList<>();
            categories.put(category.getAttributeValue("title"), thiscat);
        } else {
            thiscat = cat;
        }

        thiscat.add(rs);
    }

    private void loadData(Context context, File path) {
        if (path.isDirectory()) {
            File[] dataFiles = path.listFiles(new XMLFilter());
            SAXBuilder builder = new SAXBuilder();
            GeneratorDtdResolver resolver = new GeneratorDtdResolver(path);
            builder.setEntityResolver(resolver);

            for (File dataFile : dataFiles) {
                try {
                    URL url = dataFile.toURI().toURL();
                    Document nameSet = builder.build(url);
                    DocType dt = nameSet.getDocType();

                    if (dt.getElementName().equals("GENERATOR")) {
                        loadFromDocument(nameSet);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                    Toast.makeText(context, "XML Error with file " + dataFile.getName(), Toast.LENGTH_LONG).show();
                }
            }

            loadDropdowns(context);
        } else {
            Toast.makeText(context, "No data files in directory " + path.getPath(), Toast.LENGTH_LONG).show();
        }
    }

    //	Get a list of category names from the categories map
    private Vector<String> getCategoryNames() {
        Vector<String> cats = new Vector<>();
        Set<String> keySet = categories.keySet();

        for (final String key : keySet) {
            //	Ignore any category that starts with this
            if (key.startsWith("Sex:")) {
                continue;
            }

            cats.add(key);
        }

        //	Sor the selected categories before returning it
        Collections.sort(cats);

        return cats;
    }

    private void loadDropdowns(Context context) {
        //	This method now just loads the category dropdown from the list of
        //	category names
        Vector<String> cats = this.getCategoryNames();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
        adapter.addAll(cats);
        cbCategory.setAdapter(adapter);

        this.loadGenderDD();
        this.loadCatalogDD(context);
    }

    private void loadFromDocument(Document nameSet)
            throws DataConversionException {
        Element generator = nameSet.getRootElement();
        List<?> rulesets = generator.getChildren("RULESET");
        List<?> lists = generator.getChildren("LIST");
        ListIterator<?> listIterator = lists.listIterator();

        while (listIterator.hasNext()) {
            Element list = (Element) listIterator.next();
            loadList(list);
        }

        for (final Object ruleset : rulesets) {
            Element ruleSet = (Element) ruleset;
            RuleSet rs = loadRuleSet(ruleSet);
            allVars.addDataElement(rs);
        }
    }

    private String loadList(Element list) throws DataConversionException {
        pcgen.core.doomsdaybook.DDList dataList =
                new pcgen.core.doomsdaybook.DDList(allVars, list
                        .getAttributeValue("title"), list.getAttributeValue("id"));
        List<?> elements = list.getChildren();

        for (final Object element : elements) {
            Element child = (Element) element;
            String elementName = child.getName();

            if (elementName.equals("VALUE")) {
                WeightedDataValue dv =
                        new WeightedDataValue(child.getText(), child
                                .getAttribute("weight").getIntValue());
                List<?> subElements = child.getChildren("SUBVALUE");

                for (final Object subElement1 : subElements) {
                    Element subElement = (Element) subElement1;
                    dv.addSubValue(subElement.getAttributeValue("type"),
                            subElement.getText());
                }

                dataList.add(dv);
            }
        }

        allVars.addDataElement(dataList);

        return dataList.getId();
    }

    private String loadRule(Element rule, String id)
            throws DataConversionException {
        Rule dataRule =
                new Rule(allVars, id, id, rule.getAttribute("weight")
                        .getIntValue());
        List<?> elements = rule.getChildren();

        for (final Object element : elements) {
            Element child = (Element) element;
            String elementName = child.getName();

            if (elementName.equals("GETLIST")) {
                String listId = child.getAttributeValue("idref");
                dataRule.add(listId);
            } else if (elementName.equals("SPACE")) {
                SpaceRule sp = new SpaceRule();
                allVars.addDataElement(sp);
                dataRule.add(sp.getId());
            } else if (elementName.equals("HYPHEN")) {
                HyphenRule hy = new HyphenRule();
                allVars.addDataElement(hy);
                dataRule.add(hy.getId());
            } else if (elementName.equals("CR")) {
                CRRule cr = new CRRule();
                allVars.addDataElement(cr);
                dataRule.add(cr.getId());
            } else if (elementName.equals("GETRULE")) {
                String ruleId = child.getAttributeValue("idref");
                dataRule.add(ruleId);
            }
        }

        allVars.addDataElement(dataRule);

        return dataRule.getId();
    }

    private RuleSet loadRuleSet(Element ruleSet) throws DataConversionException {
        RuleSet rs =
                new RuleSet(allVars, ruleSet.getAttributeValue("title"),
                        ruleSet.getAttributeValue("id"), ruleSet
                        .getAttributeValue("usage"));
        List<?> elements = ruleSet.getChildren();
        ListIterator<?> elementsIterator = elements.listIterator();
        int num = 0;

        while (elementsIterator.hasNext()) {
            Element child = (Element) elementsIterator.next();
            String elementName = child.getName();

            if (elementName.equals("CATEGORY")) {
                loadCategory(child, rs);
            } else if (elementName.equals("RULE")) {
                rs.add(loadRule(child, rs.getId() + num));
            }

            num++;
        }

        return rs;
    }

    private void loadStructureDD(Context context) {
        ArrayAdapter<DataElement> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);

        if (chkStructure.isSelected()) {
            cbStructure.setAdapter(adapter);
            cbStructure.setEnabled(false);
        } else {
            Vector<DataElement> struct = new Vector<>();

            for (String key : ((RuleSet) cbCatalog.getSelectedItem())) {
                try {
                    struct.add(allVars.getDataElement(key));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }

            adapter.addAll(struct);
            cbStructure.setAdapter(adapter);
            cbStructure.setEnabled(true);
        }
    }

    /**
     * @return the generated name chosen by the user
     */
    public String getChosenName() {
        return name.getText().toString();
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return (String) cbSex.getSelectedItem();
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        if (gender != null && cbSex != null) {
            cbSex.setSelection(genderAdapter.getPosition(gender));
        }
    }

    /**
     * The Class <code>GeneratorDtdResolver</code> is an EntityResolver implementation
     * for use with a SAX parser. It forces the generator.dtd to be read from a
     * known location.
     */
    public static class GeneratorDtdResolver implements EntityResolver {

        private final File parent;

        /**
         * Create a new instance of GeneratorDtdResolver to read the
         * generator.dtd from a specific directory.
         *
         * @param parent The parent directory holding generator.dtd
         */
        GeneratorDtdResolver(File parent) {
            this.parent = parent;

        }

        @Override
        public InputSource resolveEntity(String publicId, String systemId) {
            if (systemId.endsWith("generator.dtd")) {
                // return a special input source
                InputStream dtdIn;
                try {
                    dtdIn =
                            new FileInputStream(new File(parent,
                                    "generator.dtd"));
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "GeneratorDtdResolver.resolveEntity failed", e);
                    return null;

                }
                return new InputSource(dtdIn);
            } else {
                // use the default behaviour
                return null;
            }
        }
    }
}
