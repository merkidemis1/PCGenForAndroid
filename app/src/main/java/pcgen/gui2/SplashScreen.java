/*
 * Copyright 2009 Connor Petty <cpmeister@users.sourceforge.net>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
package pcgen.gui2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.redlightning.pcgen.R;

import pcgen.system.PCGenTask;
import pcgen.system.PCGenTaskEvent;
import pcgen.system.PCGenTaskListener;

/**
 * PCGen's splash screen which is shown upon startup.
 *
 * @see pcgen.system.Main
 */
public class SplashScreen extends View implements PCGenTaskListener
{

	private final ProgressBar loadProgress;
	private final TextView loadingLabel;

	public SplashScreen(Context context)
	{
	    super(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View layout = inflater.inflate(R.layout.fragment_splash, null);
        this.loadProgress = layout.findViewById(R.id.splash_progressBar);
		this.loadingLabel = layout.findViewById(R.id.splash_status);
	}

	/**
	 * Sets the message to display
	 *
	 * @param text the message to display
	 */
	public void setMessage(String text)
	{
		loadingLabel.setText(text);
	}


	/**
	 * This updates the progress bar with the latest task progress information.
	 * It is not assumed that this method will be called on the Event Dispatch
	 * thread so UI updates are added to the Event Dispatch queue so that they
	 * are handled appropriately. To make sure that update requests do not
	 * overwhelm the UI thread a {@code dirty} flag is used to make sure
	 * that the multiple UI update requests are not queued at the same time.
	 *
	 * @param event a PCGenTaskEvent
	 */
	@Override
	public void progressChanged(final PCGenTaskEvent event)
	{
        PCGenTask task = event.getSource();
        loadProgress.setMax(task.getMaximum());
        loadProgress.setProgress(task.getProgress());
	}

	@Override
	public void errorOccurred(PCGenTaskEvent event)
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
